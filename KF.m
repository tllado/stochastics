% KF(F,e,P,z,G,H,Q,R,dt)
% 
% A standard Kalman Filter.
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-05-12
% 
% F = system matrix
% e = estimated state
% P = estimation covariance
% z = measured state
% G = noise model
% H = measurement model
% q = system noise strength
% r = measurement noise strength
% dt = timestep size

function [e,P] = KF(F,e,P,z,G,H,q,r,dt)
    eM = e + F*e*dt;                    % expected value update
    PM = P + (F*P + P*F' + G*q*G')*dt;  % expected variance update
    K = PM*H'/(H*PM*H' + r);            % Kalman gains
    e = eM + K*(z - H*eM);              % measurement update
    P = PM - K*H*PM;                    % error covariance update
end

% End of file