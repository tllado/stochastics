% demoEKF.m
% 
% A comparison of a Kalman Filter and Extended Kalman Filter estimating the 
% full state of a simulated pendulum with gaussian friction
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-05-19

clear variables;

% User Variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
g = 9.8;    % gravity       (m/sec^2)
m = 1;      % mass          (kg)
L = 1;      % length        (m)
T = 0.5;    % time constant (sec)
b = 0.05;   % friction      (N-m-sec)
theta0 = 1; % initial angle (rad)
tF = 30;    % final time    (sec)
dt = .01;   % timestep      (sec)
q = 1;      % system noise strength
r = 0.1;    % sensor noise strength

% System Model
% written as function due to non-linear elements
F = @(x) [  0                   1/m/L^2*x(2)    0           ;
            -m*g*L*sin(x(1))    -b/m/L^2*x(2)   1*x(3)      ;
            0                   0               -1/T*x(3)   ];
% Linearized System Model
FL = [  0       1/m/L^2     0       ;
        -m*g*L  -b/m/L^2    1       ;
        0       0           -1/T    ];
% Partial derivative of System w.r.t. x
% written as function due to non-linear elements
FP = @(x) [ 0                   1/m/L^2     0       ;
            -m*g*L*cos(x(1))    -b/m/L^2    1       ;
            0                   0           -1/T    ];

G = [0; 0; 1];  % noise model
H = [1 0 0];    % measurement model
Q = 2*q^2/T;    % system noise magnitude
R = r^2;        % sensor noise magnitude

% Program Variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;                % time
x = zeros(3,length(t));     % actual state
    x(1,1) = theta0;
z = zeros(1,length(t));     % measured state

eKF = zeros(3,length(t));   % estimated state, Kalman Filter
PKF = 1;                    % estimation covariance, Kalman Filter

eEKF = zeros(3,length(t));  % estimated state, Extended Kalman Filter
PEKF = 1;                   % estimation covariance, Extended Kalman Filter

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    waitbar(ii/length(t),bar);
    
    % System
    dBeta = randn*sqrt(Q);              % system noise
    dx = sum(F(x(:,ii-1)),2) + G*dBeta;
    x(:,ii) = x(:,ii-1) + dx*dt;        % Euler integration of system
    
    % Measurement
    v = randn*sqrt(R);      % sensor noise
    z(ii) = x(1,ii) + v;    % sensor measurements

    % Estimation
    [eKF(:,ii), PKF] = KF(FL,eKF(:,ii-1),PKF,z(ii),G,H,Q,R,dt);         % KF
    [eEKF(:,ii),PEKF] = EKF(F,FP,eEKF(:,ii-1),PEKF,z(ii),G,H,Q,R,dt);   % EKF
end

% Plot Results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    colorsDefault = get(gca,'colororder');
    yMax = max(abs([z x(1,:) eKF(1,:) eEKF(1,:)]));
    subplot(3,2,1)
        hold on;
        plotM = plot(t,z,        'color',colorsDefault(3,:));
        plotA = plot(t,x(1,:),   'color',colorsDefault(1,:));
        plotE = plot(t,eKF(1,:), 'color',colorsDefault(2,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular position (rad)');
        legend([plotA plotE plotM], '\theta_{actual}', ...
            '\theta_{estimated}', '\theta_{measured}');
        title('KF Estimation');
        hold off;
    subplot(3,2,2)
        hold on;
        plotM = plot(t,z,        'color',colorsDefault(3,:));
        plotA = plot(t,x(1,:),   'color',colorsDefault(1,:));
        plotE = plot(t,eEKF(1,:), 'color',colorsDefault(2,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular position (rad)');
        legend([plotA plotE plotM], '\theta_{actual}', ...
            '\theta_{estimated}', '\theta_{measured}');
        title('EKF Estimation');
        hold off;
    yMax = max(abs([x(2,:) eKF(2,:) eEKF(2,:)]));
    subplot(3,2,3)
        plot(t,x(2,:), t,eKF(2,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular velocity (rad/sec)');
        legend('\omega_{actual}', '\omega_{estimated}');
    subplot(3,2,4)
        plot(t,x(2,:), t,eEKF(2,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular velocity (rad/sec)');
        legend('\omega_{actual}', '\omega_{estimated}');
    yMax = max(abs([x(3,:) eKF(3,:) eEKF(3,:)]));
    subplot(3,2,5)
        plot(t,x(3,:), t,eKF(3,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular acceleration (rad/sec^2)');
        legend('\tau_{actual}', '\tau_{estimated}');
    subplot(3,2,6)
        plot(t,x(3,:), t,eEKF(3,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular acceleration (rad/sec^2)');
        legend('\tau_{actual}', '\tau_{estimated}');
close(bar);

% Compare Errors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
errorKF = (var(eKF - x)/var(x));
disp(['Kalman Filter error = ' num2str(errorKF*100) '%']);
errorEKF = (var(eEKF - x)/var(x));
disp(['Extended Kalman Filter error = ' num2str(errorEKF*100) '%']);

% End of File %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%