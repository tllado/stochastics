% UKFDemo.m
%
% A comparison of a Kalman Filter and Extended Kalman Filter estimating a 
% simulated pendulum with gaussian friction.
%
% Written by Travis Llado, travisllado@utexas.edu, 2018-05-05

clear variables;

% User Variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
g = 9.8;    % m/sec^2
m = 1;      % kg
L = 1;      % m
T = 0.5;    % sec
b = 0.05;   % N-m-sec
theta0 = 1; % rad
dt = 0.01;  % sec
tF = 10;    % sec
q = 0.01;   % system noise
r = 0.02;   % sensor noise

% System Model
% written as function due to non-linear elements
F = @(x) -sin(x);
% Partial derivative of System w.r.t. x
% written as function due to non-linear elements
FP = @(x) -cos(x);
G = 1;  % Noise Model
H = 1;  % Measurement Model

% Program Variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;            % time
x = zeros(1,length(t)); % actual state
    x(1) = theta0;
z = zeros(1,length(t)); % measured state

xEKF = zeros(1,length(t));  % estimated state,  Extended Kalman Filter
PEKF = 1;                   % covariance,       Extended Kalman Filter

xUKF = zeros(1,length(t));  % estimated state,  Unscented Kalman Filter
PUKF = 1;                   % covariance,       Unscented Kalman Filter

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    waitbar(ii/length(t),bar);
    
    % System
    dBeta = randn*sqrt(q);      % system noise
    dx = F(x(ii-1)) + dBeta;
    x(ii) = x(ii-1) + dx*dt;    % Euler integration of system
    
    % Measurement
    v = randn*sqrt(r);  % sensor noise
    z(ii) = x(ii) + v;  % sensor measurement

    % Estimation
    [xEKF(ii), PEKF] = EKF(F,FP,xEKF(ii-1),PEKF,z(ii),G,H,q,r,dt);  % EKF est.
    [xUKF(ii), PUKF] = UKF(F,xUKF(ii-1),PUKF,z(ii),q,r);            % UKF est.
end

% Plot Results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    hold on
        plot(t,z, t,xEKF, t,xUKF);
        plot(t,x, 'LineWidth', 2);
        xlabel('t (sec)');
        ylabel('\theta (rad)');
        legend('\theta_{measured}', '\theta_{EKF}', '\theta_{UKF}', ...
            '\theta_{actual}');
        title('Unscented vs Extended Kalman Filter');
    hold off;
close(bar);

% End of file