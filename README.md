Here we have a few simple examples of stochastic estimation and control tools, including:

- Kalman Filters

- Extended Kalman Filters

- Unscented Kalman Filters

- Statistical Linearization Filters

- Linear Quadratic Gaussian Controllers

all implemented using simulated system data in MATLAB

I plan on rewriting all of these in Python, as I'm attempting to cease use of MATLAB

I also plan on adding examples of particle filters and gaussian processes