% EKF(F,FP,e,P,z,G,H,Q,R,dt)
% 
% A standard Extended Kalman Filter
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-05-19
% 
% F = system matrix
% FP = partial derivative of system matrix with respect to x
% e = estimated state
% P = estimation covariance
% z = measured state
% G = noise model
% H = measurement model
% q = system noise strength
% r = measurement noise strength
% dt = timestep size

function [e,P] = EKF(F,FP,e,P,z,G,H,q,r,dt)
    eM = e + sum(F(e),2)*dt;                    % expected value update
    PM = P + (FP(e)*P + P*FP(e)' + G*q*G')*dt;  % expected variance update
    K = PM*H'/(H*PM*H' + r);                    % Kalman gains
    e = eM + K*(z - H*eM);                      % measurement update
    P = PM - K*H*PM;                            % error covariance update
end

% End of File