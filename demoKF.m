% demoKF.m
% 
% A Kalman Filter is used to estimate the state of a simulated pendulum with 
% gaussian friction
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-05-19

clear variables;

% User Variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
g = 9.8;    % gravity       (m/sec^2)
m = 1;      % mass          (kg)
L = 1;      % length        (m)
T = 0.5;    % time constant (sec)
b = 1;      % friction      (N-m-sec)
theta0 = 1; % initial angle (rad)
tF = 30;    % final time    (sec)
dt = .01;   % timestep      (sec)
q = 1;      % system noise strength
r = 0.1;    % sensor noise strength

% System Model
% written as function due to non-linear elements
F = @(x) [  0,                  1/m/L^2*x(2),   0           ;
            -m*g*L*sin(x(1)),   -b/m/L^2*x(2),  1*x(3)      ;
            0,                  0,              -1/T*x(3)   ];
% Linearized System Model
FL = [  0       1/m/L^2     0       ;
        -m*g*L  -b/m/L^2    1       ;
        0       0           -1/T    ];

G = [0; 0; 1];  % noise model
H = [1 0 0];    % measurement model
Q = 2*q^2/T;    % system noise magnitude
R = r^2;        % sensor noise magnitude

% Program Variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;                % time
x = zeros(3,length(t));     % actual state
    x(1,1) = theta0;
z = zeros(1,length(t));     % measured state
e = zeros(3,length(t));     % estimated state, Kalman Filter
P = 1;                      % estimation covariance, Kalman Filter

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    waitbar(ii/length(t),bar);
    
    % System
    dBeta = randn*sqrt(Q);              % system noise
    dx = sum(F(x(:,ii-1)),2) + G*dBeta;
    x(:,ii) = x(:,ii-1) + dx*dt;        % Euler integration of system
    
    % Measurement
    v = randn*sqrt(R);      % sensor noise
    z(ii) = x(1,ii) + v;    % sensor measurements

    % Estimation
    [e(:,ii), P] = KF(FL,e(:,ii-1),P,z(ii),G,H,Q,R,dt); % Kalman Filter
end

% Plot Results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    colorsDefault = get(gca,'colororder');
    subplot(3,1,1)
        hold on;
        plotM = plot(t,z,       'color',colorsDefault(3,:));
        plotA = plot(t,x(1,:),  'color',colorsDefault(1,:));
        plotE = plot(t,e(1,:),  'color',colorsDefault(2,:));
        xlabel('time (sec)');
        ylabel('angular position (rad)');
        legend([plotA plotE plotM], '\theta_{actual}', ...
            '\theta_{estimated}', '\theta_{measured}');
        hold off;
    subplot(3,1,2)
        plot(t,x(2,:), t,e(2,:));
        xlabel('time (sec)');
        ylabel('angular velocity (rad/sec)');
        legend('\omega_{actual}', '\omega_{estimated}');
    subplot(3,1,3)
        plot(t,x(3,:), t,e(3,:));
        xlabel('time (sec)');
        ylabel('angular acceleration (rad/sec^2)');
        legend('\tau_{actual}', '\tau_{estimated}');
close(bar);

% End of File %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%