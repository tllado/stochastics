% demoLQG.m
% 
% A simulation demonstrating Linear Quadratic Gaussian control of a rotating 
% mass with a random torque load and a controllable torque input
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-05-12

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
K = 1;      % Spring stiffness      (Nm/rad)
I = 1;      % Moment of Intertia    (kg-m^2)
q = 1;      % system noise strength 
r = 0.01;   % sensor noise strength 
dt = 0.01;  % time resolution       (s)
tF = 100;   % final time            (s)

F = [   0   1/I 0   ;   % System Model
        0   0   -K  ;
        0   1/I 0   ];
G = [0; 0; -1];         % Noise Input Model
H = [1 0 0];            % Observation Model
Q = diag([1 0 0]);      % State Weights
R = r;                  % Input Weights

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % time

xN = zeros(3,length(t));    % actual state,     natural
zN = zeros(1,length(t));    % measured state,   natural
eN = zeros(3,length(t));    % estimated state,  natural
PN = zeros(3,3,length(t));  % covariance,       natural

xC = zeros(3,length(t));    % actual state,     controlled
zC = zeros(1,length(t));    % measured state,   controlled
eC = zeros(3,length(t));    % estimated state,  controlled
PC = zeros(3,3,length(t));  % covariance,       controlled

[~, ~, K] = care(F,G,Q,R);  % Gain Matrix from Riccati solution
W = F - G*K;                % LQG Control Matrix

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    waitbar(ii/length(t),bar);
    
    % System
    dBeta = randn*sqrt(q);          % system noise
    dx = F*xN(:,ii-1) + G*dBeta;    % natural
    xN(:,ii) = xN(:,ii-1) + dx*dt;  % natural
    dx = W*xC(:,ii-1) + G*dBeta;    % controlled
    xC(:,ii) = xC(:,ii-1) + dx*dt;  % controlled

    % Measurement
    v = randn*r;            % sensor noise
    zN(ii) = xN(1,ii) + v;  % natural
    zC(ii) = xC(1,ii) + v;  % controlled

    % Estimation
    [eN(:,ii),PN(:,:,ii)] = ...
        KF(F,eN(:,ii-1),PN(:,:,ii-1),zN(ii),G,H,q,r,dt);    % natural
    [eC(:,ii),PC(:,:,ii)] = ...
        KF(W,eC(:,ii-1),PC(:,:,ii-1),zC(ii),G,H,q,r,dt);    % controlled
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    colorsDefault = get(gca,'colororder');
    yMax = max(abs([zN xN(1,:) eN(1,:) zC xC(1,:) eC(1,:)]));
    subplot(3,2,1)
        hold on;
        plotM = plot(t,zN,      'color',colorsDefault(3,:));
        plotA = plot(t,xN(1,:), 'color',colorsDefault(1,:));
        plotE = plot(t,eN(1,:), 'color',colorsDefault(2,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular position (rad)');
        legend([plotA plotE plotM], '\theta_{actual}', ...
            '\theta_{estimated}', '\theta_{measured}');
        title('Natural Response');
        hold off;
    subplot(3,2,2)
        hold on;
        plotM = plot(t,zC,      'color',colorsDefault(3,:));
        plotA = plot(t,xC(1,:), 'color',colorsDefault(1,:));
        plotE = plot(t,eC(1,:), 'color',colorsDefault(2,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular position (rad)');
        legend([plotA plotE plotM], '\theta_{actual}', ...
            '\theta_{estimated}', '\theta_{measured}');
        title('LQG-Controlled Response');
        hold off;
    yMax = max(abs([xN(2,:) eN(2,:) xC(2,:) eC(2,:)]));
    subplot(3,2,3)
        plot(t,xN(2,:), t,eN(2,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular velocity (rad/sec)');
        legend('\omega_{actual}', '\omega_{estimated}');
    subplot(3,2,4)
        plot(t,xC(2,:), t,eC(2,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular velocity (rad/sec)');
        legend('\omega_{actual}', '\omega_{estimated}');
    yMax = max(abs([xN(3,:) eN(3,:) xC(3,:) eC(3,:)]));
    subplot(3,2,5)
        plot(t,xN(3,:), t,eN(3,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular acceleration (rad/sec^2)');
        legend('\tau_{actual}', '\tau_{estimated}');
    subplot(3,2,6)
        plot(t,xC(3,:), t,eC(3,:));
        ylim([-yMax yMax]);
        xlabel('time (sec)');
        ylabel('angular acceleration (rad/sec^2)');
        legend('\tau_{actual}', '\tau_{estimated}');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%