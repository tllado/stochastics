% Unscented Kalman Filter
% 
% Created by Travis Llado, travisllado@utexas.edu
% 2018-05-05

function [e,P] = UKF(F,P,e,z,q,r)
    el = 1;
    alpha = 1e-3;
    kappa = 0;
    beta = 2;
    lamda = alpha^2*(el+kappa) - el;
    wM = [  lamda/(el+lamda)                    1/(2*(el+lamda))    1/(2*(el+lamda))    ];
    wC = [  lamda/(el+lamda)+(1-alpha^2+beta)   1/(2*(el+lamda))    1/(2*(el+lamda))    ];

    xA = [  e   e+sqrt((el+lamda)*P)    e-sqrt((el+lamda)*P)    ];

    xK = [ F(xA(1))   F(xA(2))   F(xA(3))   ];
    xM = sum(wM.*xK);
    Pxx = (xK - xM)*diag(wC)*(xK - xM)' + q;
    
    zK = [ xK(1)  xK(2)  xK(3)  ];
    zM = sum(wM.*zK);
    Pyy = (zK - zM)*diag(wC)*(zK - zM)' + r;

    Pxy = (xK - xM)*diag(wC)*(zK - zM)';
    K = Pxy/Pyy;
    e = xM + K*(z - zM);
    P = Pxx - K*Pxy';
end

% End of file