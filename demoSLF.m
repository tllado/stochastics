% demoSLF.m
% 
% A comparison of an Extended Kalman Filter and Statistical Linearization 
% Filter estimating the full state of a simulated pendulum
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-05-20

clear variables;

% User Variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T = 1;      % time constant (sec)
q = 500;    % system noise strength
r = 0.1;    % sensor noise strength

tF = 30;    % final time    (sec)
dt = 0.01;  % timestep      (sec)

% System Model
% written as function due to non-linear elements
F = @(x) [  0   1*x(2)               0   ;
            0   -T*sign(x(2))   1*x(3)   ;
            0   0               -1*x(3)  ];
% Partial derivative of System w.r.t. x
% written as function due to non-linear elements
FP = @(x) [ 0   1   0   ;
            0   0   1   ;
            0   0   -1  ];

G = [0; 0; 1];  % noise model
H = [1 0 0];    % measurement model

% Program Variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;                % time
x = zeros(3,length(t));     % actual state
z = zeros(1,length(t));     % measured state

eEKF = zeros(3,length(t));  % estimated state, Extended Kalman Filter
PEKF = eye(3);              % estimation covariance, Extended Kalman Filter

eSLF = zeros(3,length(t));  % estimated state, Statistical Linearization Filter
PSLF = eye(3);              % estimation covariance, Statistical Linearizat ...

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    waitbar(ii/length(t),bar);
    
    % Simulation
    dBeta = randn*sqrt(q);              % system noise
    dx = sum(F(x(:,ii-1)),2) + G*dBeta;
    x(:,ii) = x(:,ii-1) + dx*dt;        % Euler integration of system
    
    % Measurement
    v = randn*sqrt(r);      % sensor noise
    z(ii) = x(1,ii) + v;    % sensor measurements

    % Estimation
    [eEKF(:,ii),PEKF] = EKF(F,FP,eEKF(:,ii-1),PEKF,z(ii),G,H,q,r,dt);   % EKF

    % The Statistical Linearization Filter requires the creation of custom 
    % equations for every system, so there is no point in creating a generic 
    % function for this one case.
    FM22 = -T*erf(eSLF(2,ii-1)/sqrt(2*PSLF(2,2)));
    FM = [  0   eSLF(2,ii-1)    0               ;
            0   FM22            eSLF(3,ii-1)    ;
            0   0               -eSLF(3,ii-1)   ];
    nF22 = -2*T/sqrt(PSLF(2,2))*(1/sqrt(2*pi)*exp(-x(2)^2/(2*PSLF(2,2))));
    nF = [  0   1       0   ;
            0   nF22    1   ;
            0   0       -1  ];
    eM = eSLF(:,ii-1) + sum(FM,2)*dt;
    PM = PSLF + (nF*PSLF + PSLF*nF' + G*q*G')*dt;
    nH = H;
    h = eM(1);
    K = (PM*nH')/(nH*PM*nH' + r);
    eSLF(:,ii) = eM + K*(z(ii) - h);
    PSLF = (eye(3) -K*nH)*PM;
end

% Plot Results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(3,1,1)
        plot(t,x(1,:), t,eEKF(1,:), t,eSLF(1,:));
        xlabel('time (sec)');
        ylabel('angular position (rad)');
        legend('\theta_{actual}', '\theta_{est,EKF}', '\theta_{est,SLF}');
    subplot(3,1,2)
        plot(t,x(2,:), t,eEKF(2,:), t,eSLF(2,:));
        xlabel('time (sec)');
        ylabel('angular velocity (rad/sec)');
        legend('\omega_{actual}', '\omega_{est,EKF}', '\omega_{est,SLF}');
    subplot(3,1,3)
        plot(t,x(3,:), t,eEKF(3,:), t,eSLF(3,:));
        xlabel('time (sec)');
        ylabel('angular acceleration (rad/sec^2)');
        legend('\tau_{actual}', '\tau_{est,EKF}', '\tau_{est,SLF}');
close(bar);

% Comparison %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
errorEKF = (var(eEKF(1,:) - x(1,:))/var(x(1,:)));
disp(['Extended Kalman Filter error = ' num2str(errorEKF*100) '%']);
errorSLF = (var(eSLF(1,:) - x(1,:))/var(x(1,:)));
disp(['Statistical Linearization Filter error = ' num2str(errorSLF*100) '%']);

% End of File %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%